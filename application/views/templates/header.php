<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Testing Upload Image</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="<?php echo base_url(); ?>image/png" href="assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/metisMenu.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slicknav.min.css">
    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- others css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/typography.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/default-css.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
    <link href="<?php echo base_url(); ?>assets/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>assets/Chart.js"></script>
    <style>
        div.background {
            width: 500px;
            height: 250px;
            background: url(klematis.jpg) repeat;
            border: 2px solid black;
        }

        div.transbox {
            width: 400px;
            height: 180px;
            margin: 30px 50px;
            background-color: #ffffff;
            border: 1px solid black;
            opacity: 0.6;
            filter: alpha(opacity=60);
            /* For IE8 and earlier */
        }

        div.transbox p {
            margin: 30px 40px;
            font-weight: bold;
            color: #000000;
        }

        .showNot {
            display: none;
        }

        .custom-select {
            width: 60px !important;
        }

        @media print {
            .page-title-area {
                display: none !important;
                opacity: 0 !important;
            }

            .sidebar-menu {
                width: 0px !important;
                display: none !important;
                opacity: 0 !important;
            }
        }

        @media screen and (max-width: 700px) {
            .sidebar-menu {
                left: 0px !important;
                display: none;
            }

            .wtfMargin {
                margin-top: -60px;
            }

            .showNot {
                display: block;
            }

            .metismenu {
                overflow: hidden;
            }
        }
    </style>
    <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
