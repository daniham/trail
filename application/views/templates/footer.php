  <footer>
      <div class="footer-area" id="diprint">
          <p>©Copyright © DANI HAMDANI</p>
      </div>
  </footer>
 
  <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-2.2.4.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/jquery.slicknav.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/demo/datatables-demo.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/demo/chart-area-demo.js"></script>
  <script src="<?= base_url('assets/sweetalert2-8.5.0/sweetalert2.all.min.js'); ?>"></script>
  <script src="<?= base_url('assets/sweetalert2-8.5.0/myscript.js'); ?>"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/datatables/jquery.dataTables.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/datatables/dataTables.bootstrap4.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://cdn.zingchart.com/zingchart.min.js"></script>
  <script>
      zingchart.MODULESDIR = "https://cdn.zingchart.com/modules/";
      ZC.LICENSE = ["569d52cefae586f634c54f86dc99e6a9", "ee6b7db5b51705a13dc2339db3edaf6d"];
  </script>

  <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>

  <script>
      function myFunction() {
          window.print();
      }

      document.getElementsByClassName("slimScrollDiv").style.height = "800px !important";
      document.getElementsByClassName("menu-inner").style.height = "800px !important";

      function showMenu() {
          if (document.getElementById('main-sidemenu').style.display === 'none') {
              document.getElementById('main-sidemenu').style.display = 'block';
          } else {
              document.getElementById('main-sidemenu').style.display = 'none';
          }
      }
  </script>
  </body>

  </html>