<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MainModel extends CI_Model
{
	public function getAllDataGalery()
	{
		return $this->db->get('galery')->result_array();
	}
	public function hapusDataGalery($id_promo)
	{
		$this->db->delete('galery', ['id' => $id_promo]);
	}

	public function ubahDataGalery()
	{
		$upload = $this->MainModel->upload();
		$data = [
			'deskripsi' => htmlspecialchars($this->input->post('deskripsi', ENT_QUOTES, 'UTF-8', true))
		];
		if ($upload['file']['file_name'] != null) {
			$data = [
				'deskripsi' => htmlspecialchars($this->input->post('deskripsi', ENT_QUOTES, 'UTF-8', true)),
				'nama_file' => $upload['file']['file_name']
			];
		}
		$data = $this->security->xss_clean($data);
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('galery', $data);
	}

	public function getDataGalery($id)
	{
		return $this->db->get_where('galery', ['id' => $id])->row_array();
	}

	public function tambahDataGalery()
	{
		$upload = $this->MainModel->upload();
		$data = [
			'deskripsi' => htmlspecialchars($this->input->post('deskripsi', ENT_QUOTES, 'UTF-8', true)),
			'nama_file' => $upload['file']['file_name'],
			'create_date' => htmlspecialchars($this->input->post('create_date', ENT_QUOTES, 'UTF-8', true))
		];
		$data = $this->security->xss_clean($data);
		return $this->db->insert('galery', $data);
	}

	// Fungsi untuk melakukan proses upload file
	public function upload()
	{
		$config['upload_path'] = './images/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']	= '2048';
		$config['remove_space'] = TRUE;

		$this->load->library('upload', $config); // Load konfigurasi uploadnya
		if ($this->upload->do_upload('input_gambar')) { // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		} else {
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}


}
