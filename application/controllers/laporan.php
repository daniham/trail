<?php
defined('BASEPATH') or exit('No direct script access allowed');

// Load library phpspreadsheet
require('./vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
// End load library phpspreadsheet

class Laporan extends CI_Controller
{

    // Load model
    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
    }

    // Main page
    public function index()
    {
        $getData = $this->UserModel->getAllDatarevenueOBJT();
        $data = array(
            'title' => 'Contoh Laporan Excel - REVENUE',
            'revenue' => $getData
        );
        $this->load->view('laporan', $data, FALSE);
    }

    // Export ke excel
    public function export()
    {
        $getData = $this->UserModel->getAllDatarevenueOBJT();
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Lovanto R.')
            ->setLastModifiedBy('Lovanto R.')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        // Add some data
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'TANGGAL INPUT')
            ->setCellValue('B1', 'SALESMAN')
            ->setCellValue('C1', 'CABANG')
            ->setCellValue('D1', 'BAGIAN')
            ->setCellValue('E1', 'UNIT ENTRI')
            ->setCellValue('F1', 'REVENUE JASA')
            ->setCellValue('G1', 'REVENUE PART')
            ->setCellValue('H1', 'REVENUE BAHAN')
            ->setCellValue('I1', 'CREATED BY')
            ->setCellValue('J1', 'CREATED DATE');

        // Miscellaneous glyphs, UTF-8
        $i = 2;
        foreach ($getData as $revenue) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $revenue->tgl_entri)
                ->setCellValue('B' . $i, $revenue->nama_karyawan)
                ->setCellValue('C' . $i, $revenue->cabang)
                ->setCellValue('D' . $i, $revenue->bagian)
                ->setCellValue('E' . $i, $revenue->unit_entri)
                ->setCellValue('F' . $i, $revenue->t_jasa)
                ->setCellValue('G' . $i, $revenue->t_bahan)
                ->setCellValue('H' . $i, $revenue->t_part)
                ->setCellValue('I' . $i, $revenue->user_create)
                ->setCellValue('J' . $i, $revenue->create_date);
            $i++;
        }

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Report Excel ' . date('d-m-Y H'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
}
