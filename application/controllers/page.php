<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends CI_Controller
{
  private $filename = "import_data"; // Kita tentukan nama filenya

  public function __construct()
  {
    parent::__construct();
    $this->load->model('MainModel');
    $this->load->helper('url', 'form');
    $this->load->library('pagination');
  }
  public function index()
  {
    $data['galery'] = $this->MainModel->getAllDataGalery();
    $this->load->view('templates/header');
    $this->load->view('galery', $data);
    $this->load->view('templates/footer');
  }
  public function galery_tambah()
  {
    $this->MainModel->tambahDataGalery();
    $this->session->set_flashdata('flash_sukses', 'BERHASIL ditambahkan');
    redirect('page/galery');
  }
  public function galery_hapus($id_promo)
  {
    $this->MainModel->hapusDataGalery($id_promo);
    $this->session->set_flashdata('flash_sukses', 'BERHASIL dihapus');
    redirect('page/galery');
  }
  public function galery_ubah($id_promo)
  {
    $data['galery'] = $this->MainModel->getDataGalery($id_promo);
    $this->load->view('templates/header');
    $this->load->view('galery_ubah', $data);
    $this->load->view('templates/footer');
  }
  public function galery_edit()
  {
    $this->MainModel->ubahDataGalery();
    $this->session->set_flashdata('flash_sukses', 'BERHASIL diubah');
    redirect('page/galery');
  }
}
