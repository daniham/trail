<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page_User extends MY_Controller
{
  private $filename = "import_data"; // Kita tentukan nama filenya

  public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel');
    $this->load->helper('url', 'form');
    //load libary pagination
    $this->load->library('pagination');
  }
   public function index(){
 
        //konfigurasi pagination
        $config['base_url'] = site_url('page_user/reportRevenueSales'); //site url
        $config['total_rows'] = $this->db->count_all('user/laporan_revenue'); //total row
        $config['per_page'] = 5;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
 
        // Membuat Style pagination untuk BootStrap v4
      $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
 
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
 
        //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
        $data['data'] = $this->UserModel->get_revenue_list($config["per_page"], $data['page']);           
 
        $data['pagination'] = $this->pagination->create_links();
 
        //load view mahasiswa view
        $this->load->view('user/laporan_revenue',$data);
    }
  public function welcome()
  {
    $this->load->view('templates/header_user');
    $this->load->view('user/welcome');
    $this->load->view('templates/footer_user');
  }
  public function grafik()
  {
    $data['entri_revenue'] = $this->UserModel->getAllDatarevenueByDate();
    $data['revenue_view'] = $this->UserModel->getAllDatapencapaian();
    $this->load->view('templates/header_user');
    $this->load->view('grafik', $data);
    $this->load->view('templates/footer_user');
  }
  public function gantiPassword()
  {
    $this->load->view('templates/header_user');
    $this->authenticated();
    $this->load->view('user/gantiPassword');
    $this->load->view('templates/footer_user');
  }
  public function gantiPasswordNow()
  {
    $this->UserModel->gantiPasswordNow();
    $this->session->set_flashdata('flash_sukses', 'BERHASIL diubah');
    redirect('page_user/welcome');
  }
   public function pencapaian()
  {
    $data['revenue_views'] = $this->UserModel->getAllDatapencapaian();
    $this->load->view('templates/header_user');
    $this->authenticated();
    $this->load->view('user/pencapaian', $data);
    $this->load->view('templates/footer_user');
  }
   public function revenue()
  {
    $data['entri_revenue'] = $this->UserModel->getAllDatarevenues($this->session->userdata('nama_user'));
    $this->load->view('templates/header_user');
    $this->authenticated();
    $this->load->view('user/revenue', $data);
    $this->load->view('templates/footer_user');
  }
   # untuk revenue
  public function revenue_tambah()
  {
    $tgl_entri = $this->input->post('tgl_entri', ENT_QUOTES, 'UTF-8', true);
    $nama_karyawan = $this->input->post('nama_karyawan', ENT_QUOTES, 'UTF-8', true);
    $cabang = $this->input->post('cabang', ENT_QUOTES, 'UTF-8', true);
    $bagian = $this->input->post('bagian', ENT_QUOTES, 'UTF-8', true);
    $unit_entri = $this->input->post('unit_entri', ENT_QUOTES, 'UTF-8', true);
    $t_jasa = $this->input->post('t_jasa', ENT_QUOTES, 'UTF-8', true);
    $t_part = $this->input->post('t_part', ENT_QUOTES, 'UTF-8', true);
    $t_bahan = $this->input->post('t_bahan', ENT_QUOTES, 'UTF-8', true);

    $this->load->library('ciqrcode'); //pemanggilan library QR CODE

    $config['cacheable']    = true; //boolean, the default is true
    $config['cachedir']     = './assets/'; //string, the default is application/cache/
    $config['errorlog']     = './assets/'; //string, the default is application/logs/
    $config['errorlog']     = './assets/'; //string, the default is application/logs/
    $config['errorlog']     = './assets/'; //string, the default is application/logs/
    $config['imagedir']     = './assets/images/'; //direktori penyimpanan qr code
    $config['quality']      = true; //boolean, the default is true
    $config['size']         = '1024'; //interger, the default is 1024
    $config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
    $config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
    $this->ciqrcode->initialize($config);

    $image_name = $cabang . $ind . $unit_entri . '.png'; //buat name dari qr code sesuai dengan nama_karyawan
    $ind = ' ';
    $params['data'] =  $tgl_entri . $ind . $nama_karyawan . $ind . $unit_entri . $ind . $t_jasa . $ind . $t_part . $ind . $t_bahan;
    $params['level'] = 'H'; //H=High
    $params['size'] = 10;
    $params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
    $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
    $this->UserModel->tambahDatarevenue($image_name);
    $this->session->set_flashdata('flash_sukses', 'BERHASIL ditambahkan');
    redirect('page_user/revenue');
  }
  public function revenue_hapus($id)
  {
    $this->UserModel->hapusDatarevenue($id);
    $this->session->set_flashdata('flash_sukses', 'BERHASIL dihapus');
    redirect('page_user/revenue');
  }
  public function revenue_ubah($id)
  {
    $data['revenue'] = $this->UserModel->getDatarevenue($id);
    $this->load->view('templates/header_user');
    $this->load->view('user/revenue_ubah', $data);
    $this->load->view('templates/footer_user');
  }
  public function revenue_edit()
  {
    $this->UserModel->ubahDatarevenue();
    $this->session->set_flashdata('flash_sukses', 'BERHASIL diubah');
    redirect('page_user/revenue');
  }
  public function target()
  {
    $data['target'] = $this->UserModel->getAllDatatarget();
    $this->load->view('templates/header_user');
    $this->authenticated();
    $this->load->view('user/target', $data);
    $this->load->view('templates/footer_user');
  }
   # pencarian data dan laporan Revenue
  public function cari_revenue()
  {
    $keyword = $this->input->get('cari', TRUE); //mengambil nilai dari form input cari
    $data['entri_revenue'] = $this->UserModel->cari_revenue($keyword); //mencari data karyawan berdasarkan inputan
    $this->load->view('templates/header_user');
    // $this->load->view('templates/sidebar');
    $this->load->view('laporan/laporan_revenue', $data); //menampilkan data yang sudah dicari
    $this->load->view('templates/footer_user');
  }

  # untuk laporan revenue
  public function reportRevenueSales()
  {
    if (isset($_POST['submit'])) {
      $tanggal1 =  $this->input->post('tanggal1');
      $tanggal2 =  $this->input->post('tanggal2');
      $data['entri_revenue'] =  $this->UserModel->laporan_berkala($tanggal1, $tanggal2, $this->session->userdata('nama_user'));
      $this->load->view('templates/header_user');
      // $this->load->view('templates/sidebar');
      $this->load->view('user/laporan_revenue', $data);
      $this->load->view('templates/footer_user');
    } elseif (isset($_POST['submit2'])) {
      $tanggal3 =  $this->input->post('tanggal3');
      $tanggal4 =  $this->input->post('tanggal4');
      $data['entri_revenue'] =  $this->UserModel->laporan_berkala1($tanggal3, $tanggal4 ,$this->session->userdata('nama_user'));
      $this->load->view('user/cetak_revenue', $data);
    } else {
      $data['entri_revenue'] =  $this->UserModel->laporan_revenue_default($this->session->userdata('nama_user'));
      $this->load->view('templates/header_user');
      // $this->load->view('templates/sidebar');
      $this->load->view('user/laporan_revenue', $data);
      $this->load->view('templates/footer_user');
    }
  } 
}
?>